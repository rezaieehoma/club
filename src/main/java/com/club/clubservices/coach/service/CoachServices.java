package com.club.clubservices.coach.service;




import com.club.clubservices.coach.domain.Coach;

import java.util.List;


public interface CoachServices {
    void upDate(List<Long> clubs, String firstName, String lastName, String phoneNumber,
                String title, String description,
                String category, Integer experience_year, Integer point, String avatarId, long userId);


    Coach findCoachByPhoneNumber(String phoneNumber);

    String findCoachAvatarId(String phoneNumber);

}
