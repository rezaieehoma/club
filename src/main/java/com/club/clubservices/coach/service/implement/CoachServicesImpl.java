package com.club.clubservices.coach.service.implement;

import com.club.clubservices.client.controller.user.UserClient;
import com.club.clubservices.client.controller.user.UserResponse;
import com.club.clubservices.club.repository.ClubRepository;
import com.club.clubservices.coach.domain.Coach;
import com.club.clubservices.coach.exception.CoachCanNotFoundException;
import com.club.clubservices.coach.repository.ClubCoachRepository;
import com.club.clubservices.coach.repository.CoachRepository;
import com.club.clubservices.coach.service.CoachServices;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class CoachServicesImpl implements CoachServices {
    private static final Logger logger = LoggerFactory.getLogger(CoachServicesImpl.class);

    private CoachRepository coachRepository;
    private ClubRepository clubRepository;
    private UserClient userClient;
    private ClubCoachRepository clubCoachRepository;



    @Override
    public void upDate(List<Long> clubs, String firstName, String lastName, String phoneNumber,
                       String title, String description,
                       String category, Integer experience_year, Integer point, String avatarId, long userId) {
        UserResponse userResponse = userClient.getUser(userId);
        Coach coach = coachRepository.findByPhoneNumber(phoneNumber)
                .orElseThrow(CoachCanNotFoundException::new);
        coach.setUser((User) userResponse);
        coach.getUser().setUsername(((User) userResponse).getUsername());
        coach.getUser().setLastName(lastName);
        coach.setTitle(title);
        coach.setDescription(description);
        coach.setCategory(category);
        coach.setExperienceYear(experience_year);
        coach.setPoint(point);
        coach.getUserId().setAvatarId(avatarId);
        coach.setCreateDate(Timestamp.valueOf(LocalDateTime.now()));


        List<Club> all = clubs.stream().map(c -> clubRepository.findById(c)
                .orElseThrow(ClubCanNotBeFoundException::new)).collect(Collectors.toList());
        List<ClubCoach> clubCoaches = all.stream().map(club -> clubCoachRepository.findByClubAndCoach(club, coach)
                .orElseGet(() -> {
                    ClubCoach build = ClubCoach.builder()
                            .club(club)
                            .coach(coach)
                            .createDate(Timestamp.valueOf(LocalDateTime.now()))
                            .build();
                    return build;
                })).collect(Collectors.toList());
        clubCoaches.forEach(
                clubCouch -> clubCoachRepository.save(clubCouch));
    }

    @Override
    public Coach findCoachByPhoneNumber(String phoneNumber) {
        return coachRepository.findByPhoneNumber(phoneNumber).orElse(null);
    }

    @Override
    public String findCoachAvatarId(String phoneNumber) {
        User user = userServices.findUserByPhoneNumber(phoneNumber);
        return user.getAvatarId();
    }


}
