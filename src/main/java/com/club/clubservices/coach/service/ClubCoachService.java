package com.club.clubservices.coach.service;



import com.club.clubservices.club.controller.club.clubcoachlist.ClubCoachResponse;
import com.club.clubservices.club.domain.Club;
import com.club.clubservices.coach.domain.coach.ClubCoach;

import java.util.List;

public interface ClubCoachService {
    ClubCoach findCouchById(long couchId);

    ClubCoach findCouchByClub(Club club);

    List<ClubCoachResponse>  findCoachByClubId(long userId);
}
