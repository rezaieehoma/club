package com.club.clubservices.coach.service.implement;

import com.club.clubservices.client.controller.user.UserClient;
import com.club.clubservices.client.controller.user.UserResponse;
import com.club.clubservices.club.controller.club.clubcoachlist.ClubCoachResponse;
import com.club.clubservices.club.domain.Club;
import com.club.clubservices.clubadmin.domain.ClubAdmin;
import com.club.clubservices.clubadmin.service.ClubAdminService;
import com.club.clubservices.coach.domain.coach.ClubCoach;
import com.club.clubservices.coach.repository.ClubCoachRepository;
import com.club.clubservices.coach.service.ClubCoachService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ClubCoachServiceImpl implements ClubCoachService {
    private ClubCoachRepository clubCoachRepository;
    private ClubAdminService clubAdminService;
    private UserClient userClient;


    @Override
    public ClubCoach findCouchById(long couchId) {

        return clubCoachRepository.findById(couchId).orElse(null);
    }

    @Override
    public ClubCoach findCouchByClub(Club club){
        return clubCoachRepository.findByClub(club).orElse(null);
    }

    @Override
    public List<ClubCoachResponse> findCoachByClubId(long userId) {
        UserResponse user= userClient.getUser(userId);
        ClubAdmin clubAdmin = clubAdminService.findByPhoneNumber(user.getPhoneNumber());
        return  clubCoachRepository.findByClubId(clubAdmin.getClub().getId()).stream().map(
                 clubCoach -> {
                     return ClubCoachResponse.builder()
                             .firstName(user.getFirstName())
                             .lastName(user.getLastName())
                             .title(clubCoach.getCoach().getTitle())
                             .category(clubCoach.getCoach().getCategory())
                             .description(clubCoach.getCoach().getDescription())
                             .phoneNumber(clubCoach.getCoach().getPhoneNumber())
                             .experienceYear(clubCoach.getCoach().getExperienceYear())
                             .point(clubCoach.getCoach().getPoint())
                             .avatarId(user.getAvatarUrl())
                             .userStatus(user.getStatus())
                             .build();
                 }
         ).collect(Collectors.toList());

    }

}
