package com.club.clubservices.coach.exception;

import com.club.clubservices.club.exception.BusinessException;

public class CoachCanNotBeDeletedException extends BusinessException {
}
