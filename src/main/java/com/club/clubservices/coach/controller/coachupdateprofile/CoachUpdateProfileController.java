package com.club.clubservices.coach.controller.coachupdateprofile;


import com.club.clubservices.client.controller.authentication.OnlineUser;
import com.club.clubservices.coach.service.CoachServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CoachUpdateProfileController {
    private CoachServices coachServices;
    private OnlineUser onlineUser;

    @Autowired
    public CoachUpdateProfileController(CoachServices coachServices, OnlineUser onlineUser) {
        this.coachServices = coachServices;
        this.onlineUser = onlineUser;
    }

    @PutMapping("/coaches/update/file")
    public void handle(@RequestBody CoachUpdateProfileRequest request) {
        coachServices.upDate(request.getClubs(), request.getFirstName(), request.getLastName()
                , request.getPhoneNumber(), request.getTitle(), request.getDescription()
                , request.getCategory(), request.getExperienceYear()
                , request.getPoint(), request.getAvatarId(), onlineUser.getUserId());
    }
}

