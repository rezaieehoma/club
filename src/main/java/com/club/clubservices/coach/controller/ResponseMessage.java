package com.club.clubservices.coach.controller;

public class ResponseMessage {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
