package com.club.clubservices.coach.controller.coachupdateprofile;

import lombok.*;

import java.util.List;


@Setter
@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class CoachUpdateProfileRequest {
    private List<Long> clubs;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String title;
    private String description;
    private String category;
    private int experienceYear;
    private int point;
    private String avatarId;
}
