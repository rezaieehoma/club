package com.club.clubservices.coach.domain.coach;

import com.club.clubservices.club.domain.Club;
import com.club.clubservices.coach.domain.Coach;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Setter
@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@IdClass(ClubCoachKey.class)
public class ClubCoach {
    @Id
    @ManyToOne
    @JoinColumn(name = "club_id")
    private Club club;
    @Id
    @ManyToOne
    @JoinColumn(name = "coach_id")
    private Coach coach;
    private Timestamp createDate;

}
