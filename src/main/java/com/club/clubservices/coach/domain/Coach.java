package com.club.clubservices.coach.domain;


import com.club.clubservices.coach.domain.coach.ClubCoach;
import lombok.*;
import org.apache.catalina.User;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Setter
@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class Coach {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @OneToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id")
    private User user;
    //    @Transient
//    private String firstName;
//    @Transient
//    private String lastName;
    private String phoneNumber;
    private String title;
    @Lob
    private String description;
    @Transient
    private String imageUrl;
    @Transient
    private String avatarId;
    private String category;
    private Integer experienceYear;
    private Integer point;
    @OneToMany(mappedBy = "coach")
    private Set<ClubCoach> clubCoaches;
    private Timestamp createDate;
}
