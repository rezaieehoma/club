package com.club.clubservices.coach.domain.coach;

import java.io.Serializable;

public class ClubCoachKey implements Serializable {
    private long club;
    private long coach;

    public long getClub() {
        return club;
    }

    public void setClub(long club) {
        this.club = club;
    }

    public long getCoach() {
        return coach;
    }

    public void setCoach(long coach) {
        this.coach = coach;
    }
}
