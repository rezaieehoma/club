package com.club.clubservices.coach.repository;

import com.club.clubservices.club.domain.Club;
import com.club.clubservices.coach.domain.Coach;
import com.club.clubservices.coach.domain.coach.ClubCoach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ClubCoachRepository extends JpaRepository<ClubCoach, Long> {
    Optional<ClubCoach> findByClubAndCoach(Club club, Coach couch);

    Optional<ClubCoach> findByClub(Club club);

    Optional<ClubCoach> findById(long clubCoachId);

    List<ClubCoach> findByClubId(long clubId);
}
