package com.club.clubservices.coach.repository;

import com.club.clubservices.coach.domain.Coach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CoachRepository extends JpaRepository<Coach, Long> {
    Optional<Coach> findByPhoneNumber(String phoneNumber);
}
