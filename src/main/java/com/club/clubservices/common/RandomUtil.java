package com.club.clubservices.common;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomUtil {
    public static String generateRandomString(Integer lang) {
        return RandomStringUtils.randomAlphabetic( lang );

    }

    public static String generateRandowmNumber(int len) {
        return RandomStringUtils.randomNumeric( len );
    }
}
