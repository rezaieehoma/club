package com.club.clubservices.common.converter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.base.Strings;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LocalDateHandler extends JsonDeserializer<Date> {
    private static final ThreadLocal<SimpleDateFormat> sdf =
            ThreadLocal.<SimpleDateFormat>withInitial(
                    () -> {
                        return new SimpleDateFormat( "dd-MM-yyyy" );
                    } );

    @Override
    public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( "dd-MM-yyyy" );
        String dateAsString = p.getText();
        try {
            if (Strings.isNullOrEmpty( dateAsString )) {
                return null;
            } else {
                return new Date( sdf.get().parse( dateAsString ).getTime() );
            }
        } catch (ParseException pe) {
            throw new RuntimeException( pe );
        }
    }
}
