package com.club.clubservices.common.type;


public enum SessionStatus {
    ONE_MONTH( "ONE_MONTH" ),
    THREE_MONTHS( "THREE_MONTHS" ),
    SIX_MONTHS( "SIX_MONTHS" ),
    YEARLY( "YEARLY" ),
    DAILY( "DAILY" );

    private String enumValue;

    SessionStatus(String enumValue) {
        this.enumValue = enumValue;
    }


}
