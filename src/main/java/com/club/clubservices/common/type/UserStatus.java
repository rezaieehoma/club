package com.club.clubservices.common.type;

public enum UserStatus {
    ACTIVE,
    BLOCK
}
