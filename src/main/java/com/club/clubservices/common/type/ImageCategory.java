package com.club.clubservices.common.type;

public enum ImageCategory {
    INFO,
    EVENT,
    CAMP
}
