package com.club.clubservices.common.type;

public enum ImageType {
    TIFF,
    JPEG,
    GIF,
    PNG
}
