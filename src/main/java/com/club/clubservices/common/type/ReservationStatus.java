package com.club.clubservices.common.type;

public enum ReservationStatus {
    RESERVE,
    CANCEL
}
