package com.club.clubservices.config.security;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AuthenticationResponse {
    private List<String> roles;
    private Long id;
}
