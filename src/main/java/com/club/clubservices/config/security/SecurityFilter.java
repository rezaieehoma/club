package com.club.clubservices.config.security;


import com.club.clubservices.client.controller.authentication.AuthenticationClient;
import com.club.clubservices.client.controller.authentication.AuthenticationRequest;
import com.club.clubservices.client.controller.authentication.OnlineUser;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class SecurityFilter extends OncePerRequestFilter {
    private final AuthenticationClient authenticationClient;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {

        String token = request.getHeader("Authorization");

        if (StringUtils.isNotEmpty(token)) {
            AuthenticationResponse authenticationResponse = authenticationClient.authenticationUploadFile(
                    AuthenticationRequest.builder()
                            .token(token.substring(7))
                            .build()
            );

            OnlineUser onlineUser = new OnlineUser();
            onlineUser.setUserId(authenticationResponse.getId());
            onlineUser.setRoles(authenticationResponse.getRoles());

            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(onlineUser, null,
                            onlineUser.getAuthorities());
            usernamePasswordAuthenticationToken.setDetails(
                    new WebAuthenticationDetailsSource().buildDetails(request)
            );

            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        }

        chain.doFilter(request, response);
    }
}