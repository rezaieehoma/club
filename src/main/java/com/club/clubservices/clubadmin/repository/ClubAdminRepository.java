package com.club.clubservices.clubadmin.repository;


import com.club.clubservices.clubadmin.domain.ClubAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface ClubAdminRepository extends JpaRepository<ClubAdmin, Long> {
  Optional<ClubAdmin> findByUserPhoneNumber(String userPhoneNumber);

  Optional<ClubAdmin> findById(long id);
}
