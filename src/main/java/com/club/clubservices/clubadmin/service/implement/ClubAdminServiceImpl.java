package com.club.clubservices.clubadmin.service.implement;

import com.club.clubservices.clubadmin.domain.ClubAdmin;
import com.club.clubservices.clubadmin.repository.ClubAdminRepository;
import com.club.clubservices.clubadmin.service.ClubAdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ClubAdminServiceImpl implements ClubAdminService {
    private ClubAdminRepository clubAdminRepository;


    @Override
    public ClubAdmin findByPhoneNumber(String phoneNumber) {
        return clubAdminRepository.findByUserPhoneNumber(phoneNumber).orElse(null);
    }


}
