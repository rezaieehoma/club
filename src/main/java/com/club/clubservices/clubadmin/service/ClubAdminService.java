package com.club.clubservices.clubadmin.service;


import com.club.clubservices.clubadmin.domain.ClubAdmin;

public interface ClubAdminService {
    ClubAdmin findByPhoneNumber(String phoneNumber);


}

