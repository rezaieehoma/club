package com.club.clubservices.clubadmin.domain;


import com.club.clubservices.club.domain.Club;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ClubAdmin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @OneToOne(targetEntity = Club.class)
    @JoinColumn(name = "club_id")
    private Club club;
    private String userPhoneNumber;
    private LocalDateTime createDate;

}
