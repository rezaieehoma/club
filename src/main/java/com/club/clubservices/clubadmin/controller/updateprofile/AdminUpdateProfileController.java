package com.club.clubservices.clubadmin.controller.updateprofile;


import com.club.clubservices.client.controller.authentication.OnlineUser;
import com.club.clubservices.clubadmin.controller.AdminClubResponseMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AdminUpdateProfileController {
    private OnlineUser onlineUser;
    private UserServices userServices;


    @PutMapping("/clubs/admin/profileupdate")
    public AdminClubResponseMessage handle(@RequestBody AdminUpdateProfileRequest request) {
        userServices.update(onlineUser.getUserId(),
                request.getFirstName(), request.getLastName(), request.getFileId());
        return AdminClubResponseMessage.builder().message("Your profile has been updated").build();
    }
}
