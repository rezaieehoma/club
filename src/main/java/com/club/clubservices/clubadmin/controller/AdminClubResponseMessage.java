package com.club.clubservices.clubadmin.controller;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AdminClubResponseMessage {
    private String message;
}
