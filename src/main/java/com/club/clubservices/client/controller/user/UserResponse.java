package com.club.clubservices.client.controller.user;

import lombok.*;



@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class UserResponse {
    private Long userId;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String avatarUrl;
    private String status;
}
