package com.club.clubservices.client.controller.authentication;

import com.club.clubservices.config.security.AuthenticationResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "core")
public interface AuthenticationClient {
    @PostMapping("/core/internal/user/userauth")
    AuthenticationResponse authenticationUploadFile(@RequestBody AuthenticationRequest request);
}
