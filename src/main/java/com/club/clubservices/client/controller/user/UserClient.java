package com.club.clubservices.client.controller.user;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "core")
public interface UserClient {
    @GetMapping(value = "/core/internal/user/{userId}")
    UserResponse getUser(@PathVariable long userId);
}
