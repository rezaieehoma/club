package com.club.clubservices.club.controller.club.listofsubclubs;

import lombok.*;

import java.sql.Time;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ClubTimeResponse {
    private Time startTime;
    private Time endTime;
    private String title;
    private String timeStatus;
    private double price;

}
