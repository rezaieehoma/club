package com.club.clubservices.club.controller.reservation.user_reservation;

import com.sportcenter.clubservice.club.controller.ResponseMessage;
import com.sportcenter.clubservice.club.domain.reservation.Reservation;
import com.sportcenter.clubservice.club.service.ClubServices;
import com.sportcenter.clubservice.club.service.ReservationServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;

@RestController
public class ReservationController {
    private static final Logger logger = LoggerFactory.getLogger( ReservationController.class );

    private ReservationServices reservationServices;
    private ClubServices clubServices;
    private OnlineUser onlineUser;
    private Timestamp timestamp;

    @Autowired
    public ReservationController(ReservationServices reservationServices, ClubServices clubServices, OnlineUser onlineUser) {
        this.reservationServices = reservationServices;
        this.clubServices = clubServices;
        this.onlineUser = onlineUser;
    }

    @PostMapping("/users/me/reservation")
    public ResponseMessage handle(@RequestBody RequestReservation request) {


        Reservation reservation = reservationServices.save(onlineUser.getUserId()
                , request.getClubTimeHoursId()
                , request.getReservationStartDate()
                , request.getClubSessionId());

        //send verification reservation to user via Email


        ResponseMessage message = new ResponseMessage();

        message.setText( "Your reservation is confirmed" );

        return message;
    }
}
