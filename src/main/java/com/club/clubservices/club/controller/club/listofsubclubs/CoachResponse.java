package com.club.clubservices.club.controller.club.listofsubclubs;


import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class CoachResponse {
    private String firstName;
    private String lastName;
    private String title;
    private String description;
    private String category;
    private Integer experienceYear;
    private Integer point;
    private String avatarId;
}
