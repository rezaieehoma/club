package com.club.clubservices.club.controller.imag;

import com.sportcenter.clubservice.club.controller.ResponseMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UploadImageController {


    @GetMapping("/users/me/message")
    public ResponseMessage handle() {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setText("Hello");
        return responseMessage;
    }
}
