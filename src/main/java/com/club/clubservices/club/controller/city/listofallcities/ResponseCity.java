package com.club.clubservices.club.controller.city.listofallcities;

public class ResponseCity {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
