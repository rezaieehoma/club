package com.club.clubservices.club.controller.club.clubsave;

import com.sportcenter.clubservice.club.controller.ResponseMessage;
import com.sportcenter.clubservice.club.service.ClubServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClubSaveController {
    private static final Logger logger = LoggerFactory.getLogger( ClubSaveController.class );
    private ClubServices services;

    @Autowired
    public ClubSaveController(ClubServices services) {
        this.services = services;
    }

    @PostMapping("/app_admin/develop/club/save")
    public ResponseMessage handle(@RequestBody ClubRequest request) {
        services.save(request.getTitle(), request.getCategoryId(), request.getAddress()
                , request.getCityId()
                , request.getLocationLat()
                , request.getLocationLong(), request.getMonthlyCost(), request.getDailyCost()
                , request.isAssortmentSport(), request.getParentClubId());

        ResponseMessage message = new ResponseMessage();
        message.setText("Club " + request.getTitle() + " Added in our system");
        return message;
    }
}
