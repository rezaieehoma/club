package com.club.clubservices.club.controller.club.clubcategorysave;


import com.club.clubservices.club.service.ClubCategoryServices;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class ClubCategoriesListController {
    private final ClubCategoryServices clubCategoryServices;

    @GetMapping(value = "/users/me/category")
    public List<ClubCategoryResponse> handle() {
        return clubCategoryServices.all().stream().map(clubCategory -> {
            return ClubCategoryResponse.builder().title(clubCategory.getTitle()).build();
        }).collect(Collectors.toList());
    }
}
