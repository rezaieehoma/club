package com.club.clubservices.club.controller.club.filterofclubs;

import com.sportcenter.clubservice.club.controller.club.listofsubclubs.ClubTimeResponse;
import com.sportcenter.clubservice.club.controller.club.listofsubclubs.CoachResponse;
import com.sportcenter.clubservice.club.domain.club.Club;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ClubFilterResponse {
    private String title;
    private String category;
    private String city;
    private String address;
    private double locationLat;
    private double locationLong;
    private double monthlyCost;
    private double dailyCost;
    private boolean isComplex;
    private Set<Club> subClubs;
    private List<String> images;
    private List<CoachResponse> couchs;
    private List<ClubTimeResponse> clubTimes;


    public static class Time {
        private String title;
        private LocalDateTime startTime;
        private LocalDateTime endTime;


        public Time(String title, LocalDateTime startTime, LocalDateTime endTime) {
            this.title = title;
            this.startTime = startTime;
            this.endTime = endTime;
        }

        public String getTitle() {
            return title;
        }

        public LocalDateTime getStartTime() {
            return startTime;
        }

        public LocalDateTime getEndTime() {
            return endTime;
        }
    }
}
