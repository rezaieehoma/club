package com.club.clubservices.club.controller.ClubSession;

import com.sportcenter.clubservice.club.service.ClubSessionServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClubSessionController {
    private ClubSessionServices clubSessionServices;
    private OnlineUser onlineUser;

    @Autowired
    public ClubSessionController(ClubSessionServices clubSessionServices, OnlineUser onlineUser) {
        this.clubSessionServices = clubSessionServices;
        this.onlineUser = onlineUser;
    }

    @PostMapping("/clubs/session")
    public void handle(@RequestBody ClubSessionRequest request) {
        clubSessionServices.save(onlineUser.getUserId(),
                request.getSessionStatus(), request.getSessionCount(), request.getPrice());
    }
}
