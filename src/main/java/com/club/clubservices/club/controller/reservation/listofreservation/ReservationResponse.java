package com.club.clubservices.club.controller.reservation.listofreservation;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sportcenter.clubservice.club.domain.session.ClubSession;
import com.sportcenter.clubservice.common.type.ReservationStatus;
import com.sportcenter.clubservice.common.type.SessionStatus;
import lombok.*;

import java.sql.Timestamp;


@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ReservationResponse {
    private String firstName;
    private String lastName;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Timestamp reservationStartDate;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Timestamp reservationEndDate;
    private ReservationStatus status;
    private SessionStatus sessionStatus;
    private int sessionCount;
    private ClubSession clubSession;


}
