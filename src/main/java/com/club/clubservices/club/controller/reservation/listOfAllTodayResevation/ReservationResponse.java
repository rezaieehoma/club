package com.club.clubservices.club.controller.reservation.listOfAllTodayResevation;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sportcenter.clubservice.common.type.ReservationStatus;
import lombok.*;

import java.sql.Timestamp;


@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ReservationResponse {
    private String firstName;
    private String lastName;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Timestamp reservationStartDate;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Timestamp reservationEndDate;
    private String title;
    private ReservationStatus status;
    private int session;

}
