package com.club.clubservices.club.controller.club.clubUpdate;

import com.sportcenter.clubservice.club.controller.ResponseMessage;
import com.sportcenter.clubservice.club.service.ClubServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClubUpdateController {
    private ClubServices clubServices;
    private OnlineUser onlineUser;

    @Autowired
    public ClubUpdateController(ClubServices clubServices, OnlineUser onlineUser) {
        this.clubServices = clubServices;
        this.onlineUser = onlineUser;
    }

    @PutMapping(value = "/clubs/changeinfo/{clubId}")
    public ResponseMessage handle(@PathVariable("clubId") long clubId, @RequestBody ClubRequest request) {
        clubServices.updateClub(request.getTitle(), request.getMonthlyCost(),
                request.getDailyCost(), clubId);

        ResponseMessage response = new ResponseMessage();
        response.setText("Club data is updated");
        return response;
    }
}
