package com.club.clubservices.club.controller.club.listofsubclubs;

import com.sportcenter.clubservice.club.service.ClubImageServices;
import com.sportcenter.clubservice.club.service.ClubServices;
import com.sportcenter.clubservice.club.service.ClubTimeHoursServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class ListOfSubClubController {
    private final ClubServices services;
    private final OnlineUser onlineUser;
    private final ClubImageServices clubImageServices;
    private final ClubTimeHoursServices clubTimeHoursServices;


    @GetMapping(value = "/clubs/subclubs")
    public List<ClubResponse> handle() {
        return services.getClub(onlineUser.getUserId()).stream().map(club -> {
            ClubResponse response = ClubResponse.builder()
                    .title(club.getTitle())
                    .categoryId(club.getCategory().getId())
                    .cityId(club.getCity().getId())
                    .address(club.getAddress())
                    .locationLat(club.getLocationLat())
                    .locationLong(club.getLocationLong())
                    .monthlyCost(club.getMonthlyCost())
                    .subClubs(null)
                    .avatarId(clubImageServices.findImageByClubId(club.getId())
                            .stream().map(image -> image.getAvatarId()).collect(Collectors.toList()) == null ? null
                            : clubImageServices.findImageByClubId(club.getId())
                            .stream().map(image -> image.getAvatarId()).collect(Collectors.toList()))
                    .coaches(club.getClubCoaches().stream().map(c -> {
                        CoachResponse couch = CoachResponse.builder()
                                .firstName(c.getCoach().getUserId().getFirstName())
                                .lastName(c.getCoach().getUserId().getLastName())
                                .title(c.getCoach().getTitle())
                                .description(c.getCoach().getDescription())
                                .category(c.getCoach().getCategory())
                                .experienceYear(c.getCoach().getExperienceYear())
                                .point(c.getCoach().getPoint())
                                .avatarId(c.getCoach().getUserId().getAvatarId())
                                .build();
                        return couch;
                    }).collect(Collectors.toList()))
                    .clubTimes(clubTimeHoursServices.findAllClubTime(club.getId()).stream().map(time -> {
                        ClubTimeResponse build = ClubTimeResponse.builder()
                                .startTime(time.getStartTime())
                                .endTime(time.getEndTime())
                                .title(time.getTitle())
                                .timeStatus(time.getTimeStatus().name())
                                .price(time.getPrice())
                                .build();
                        return build;
                    }).collect(Collectors.toList()))
                    .dailyCost( club.getDailyCost() )
                    .build();
            return response;
        } ).collect( Collectors.toList() );
    }
}
