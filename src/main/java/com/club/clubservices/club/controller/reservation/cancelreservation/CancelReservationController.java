package com.club.clubservices.club.controller.reservation.cancelreservation;

import com.sportcenter.clubservice.club.controller.ResponseMessage;
import com.sportcenter.clubservice.club.service.ReservationServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CancelReservationController {
    private ReservationServices reservationServices;
    private OnlineUser onlineUser;

    @Autowired
    public CancelReservationController(ReservationServices reservationServices, OnlineUser onlineUser) {
        this.reservationServices = reservationServices;
        this.onlineUser = onlineUser;
    }

    @PutMapping("/users/me/cancel")
    public ResponseMessage handle(@RequestBody CancelReservationRequest request) {
        reservationServices.cancelReservation(onlineUser.getUserId()
                , request.getClubTimeHoursId(), request.getReservationStartDate());

        ResponseMessage message = new ResponseMessage();
        message.setText("Your reservation is calnceld");

        return message;
    }
}
