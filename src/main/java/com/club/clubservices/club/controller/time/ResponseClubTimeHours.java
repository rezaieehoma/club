package com.club.clubservices.club.controller.time;

import com.sportcenter.clubservice.common.type.TimeStatus;
import lombok.*;

import java.sql.Time;

@Setter
@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ResponseClubTimeHours {
    private Time startTime;
    private Time endTime;
    private String title;
    private TimeStatus timeStatus;
    private double price;
}
