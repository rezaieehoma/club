package com.club.clubservices.club.controller.reservation.userresrvationlist;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sportcenter.clubservice.common.type.SessionStatus;
import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class UserReservationResponse {
    private String firstName;
    private String lastName;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Timestamp reservationStartDate;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Timestamp reservationEndDate;
    private String clubTitle;
    private SessionStatus sessionStatus;
}
