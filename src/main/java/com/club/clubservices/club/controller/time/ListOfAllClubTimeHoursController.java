package com.club.clubservices.club.controller.time;

import com.sportcenter.clubservice.club.service.ClubTimeHoursServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ListOfAllClubTimeHoursController {
    private static final Logger logger = LoggerFactory.getLogger( ClubTimeHoursController.class );

    private ClubTimeHoursServices clubTimeHoursServices;
    private OnlineUser onlineUser;

    @Autowired
    public ListOfAllClubTimeHoursController(ClubTimeHoursServices clubTimeHoursServices, OnlineUser onlineUser) {
        this.clubTimeHoursServices = clubTimeHoursServices;
        this.onlineUser = onlineUser;
    }

    @GetMapping("/users/me/timelist")
    public List<ResponseClubTimeHours> handle(@RequestParam(value = "clubId") long clubId) {
        return clubTimeHoursServices.findAllClubTime(clubId).stream().map(club -> {
            return ResponseClubTimeHours.builder()
                    .startTime(club.getStartTime())
                    .endTime(club.getEndTime())
                    .title(club.getTitle())
                    .timeStatus(club.getTimeStatus())
                    .price(club.getPrice())
                    .build();
        }).collect(Collectors.toList());

    }
}
