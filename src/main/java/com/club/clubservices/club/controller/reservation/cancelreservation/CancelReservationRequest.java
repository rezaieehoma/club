package com.club.clubservices.club.controller.reservation.cancelreservation;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.sql.Timestamp;


@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class CancelReservationRequest {
    private long clubTimeHoursId;
    @JsonFormat(shape = JsonFormat.Shape.ANY, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT")
    private Timestamp reservationStartDate;
    private int session;
}
