package com.club.clubservices.club.controller;

public class ResponseMessage {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
