package com.club.clubservices.club.controller.club.filterofclubs;


import com.sportcenter.clubservice.club.controller.club.listofsubclubs.ClubTimeResponse;
import com.sportcenter.clubservice.club.controller.club.listofsubclubs.CoachResponse;
import com.sportcenter.clubservice.club.service.ClubImageServices;
import com.sportcenter.clubservice.club.service.ClubServices;
import com.sportcenter.clubservice.club.service.ClubTimeHoursServices;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class FilterOfClubController {
    private static final Logger logger = LoggerFactory.getLogger(FilterOfClubController.class);
    private final ClubServices clubServices;
    private final ClubImageServices clubImageServices;
    private final ClubTimeHoursServices clubTimeHoursServices;


    @GetMapping(value = "/users/me/clubs")
    public List<ClubFilterResponse> handle(@RequestParam(value = "categoryId", required = false) Integer categoryId
            , @RequestParam(value = "cityId", required = false) Integer cityId) {

        return clubServices.listOfClubCategory(categoryId, cityId).stream().map(club -> {
            ClubFilterResponse response = ClubFilterResponse.builder()
                    .title(club.getTitle())
                    .category(club.getCategory().getTitle())
                    .city(club.getCity().getTitle())
                    .address(club.getAddress())
                    .locationLat(club.getLocationLat())
                    .locationLong(club.getLocationLong())
                    .monthlyCost(club.getMonthlyCost())
                    .dailyCost(club.getDailyCost())
                    .subClubs(null)
                    .images(clubImageServices.findImageByClubId(club.getId())
                            .stream().map(image -> image.getAvatarId()).collect(Collectors.toList()) == null ? null
                            : clubImageServices.findImageByClubId(club.getId())
                            .stream().map(image -> image.getAvatarId()).collect(Collectors.toList()))
                    .couchs(club.getClubCoaches().stream().map(c -> {
                        CoachResponse couch = CoachResponse.builder()
                                .firstName(c.getCoach().getUserId().getFirstName())
                                .lastName(c.getCoach().getUserId().getLastName())
                                .title(c.getCoach().getTitle())
                                .description(c.getCoach().getDescription())
                                .category(c.getCoach().getCategory())
                                .experienceYear(c.getCoach().getExperienceYear())
                                .point(c.getCoach().getPoint())
                                .avatarId(c.getCoach().getUserId().getAvatarId())
                                .build();
                        return couch;
                    }).collect(Collectors.toList()))
                    .clubTimes(clubTimeHoursServices.findAllClubTime(club.getId()).stream().map(time -> {
                        ClubTimeResponse build = ClubTimeResponse.builder()
                                .startTime(time.getStartTime())
                                .endTime(time.getEndTime())
                                .title(time.getTitle())
                                .timeStatus(time.getTimeStatus().name())
                                .price(time.getPrice())
                                .build();
                        return build;
                    }).collect(Collectors.toList()))
                    .build();

            return response;
        } ).collect( Collectors.toList() );

    }
}
