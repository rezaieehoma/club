package com.club.clubservices.club.controller.presence_absence;

import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class PresenceAbsenceRequest {
    private long reservationId;
}
