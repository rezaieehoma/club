package com.club.clubservices.club.controller.reservation.listOfAllTodayResevation;

import com.sportcenter.clubservice.club.service.ReservationServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ListOfTodayReservationController {
    private static final Logger logger = LoggerFactory.getLogger( ListOfTodayReservationController.class );

    private ReservationServices reservationServices;
    private OnlineUser onlineUser;

    @Autowired
    public ListOfTodayReservationController(ReservationServices reservationServices, OnlineUser onlineUser) {
        this.reservationServices = reservationServices;
        this.onlineUser = onlineUser;
    }

    @GetMapping("/clubs/list/today")
    public List<ReservationResponse> handle() {
        return reservationServices.listOfTodayReservation(onlineUser.getUserId()).stream().map(reserve -> {
            ReservationResponse response = ReservationResponse.builder()
                    .firstName(reserve.getUserId().getFirstName())
                    .lastName(reserve.getUserId().getLastName())
                    .reservationStartDate(reserve.getReservationStartDate())
                    .reservationEndDate(reserve.getReservationEndDate())
                    // .reservationDate( (reserve.getCreateDate()) )
                    // .title( reserve.getClubTimeHoursId().getTitle() )
                    .status(reserve.getStatus())
                    .build();

            return response;
        } ).collect( Collectors.toList() );

    }
}
