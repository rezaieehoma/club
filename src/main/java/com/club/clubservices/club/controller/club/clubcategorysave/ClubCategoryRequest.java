package com.club.clubservices.club.controller.club.clubcategorysave;

public class ClubCategoryRequest {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
