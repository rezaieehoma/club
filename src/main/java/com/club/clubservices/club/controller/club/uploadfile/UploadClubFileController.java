package com.club.clubservices.club.controller.club.uploadfile;


import com.sportcenter.clubservice.club.client.UploadFileClientResponse;
import com.sportcenter.clubservice.club.domain.image.UploadFileInfo;
import com.sportcenter.clubservice.club.service.ClubServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
public class UploadClubFileController {
    private final OnlineUser onlineUser;
    private final ClubServices clubServices;


    @PostMapping("/clubs/profile/avatar")
    public UploadFileClientResponse handle(@RequestBody MultipartFile file,
                                           @RequestParam(value = "imageCategory", required = false) String imageCategory) {
        UploadFileInfo uploadFileInfo = clubServices.uploadFileClub(onlineUser.getUserId(), file, imageCategory);

        return UploadFileClientResponse.builder()
                .id(uploadFileInfo.getId())
                .url(uploadFileInfo.getUrl())
                .build();
    }
}


