package com.club.clubservices.club.controller.reservation.userresrvationlist;

import com.sportcenter.clubservice.club.service.ReservationServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserReservationListController {
    private ReservationServices reservationServices;
    private OnlineUser onlineUser;

    @Autowired
    public UserReservationListController(ReservationServices reservationServices, OnlineUser onlineUser) {
        this.reservationServices = reservationServices;
        this.onlineUser = onlineUser;
    }

    @GetMapping("/users/me/reservation/list")
    public List<UserReservationResponse> handle() {
        return reservationServices.listOfAllUserReservation(onlineUser.getUserId()).stream().map(reservation -> {
            return UserReservationResponse.builder()
                    .clubTitle(reservation.getClubId().getTitle())
                    .firstName(reservation.getUserId().getFirstName())
                    .lastName(reservation.getUserId().getLastName())
                    .reservationStartDate(reservation.getReservationStartDate())
                    .reservationEndDate(reservation.getReservationEndDate())
                    // .sessionStatus( reservation.getClubSessionId().getSessionStatus() )
                    .build();

        } ).collect( Collectors.toList() );
    }
}
