package com.club.clubservices.club.controller.ClubSession;

import com.sportcenter.clubservice.common.type.SessionStatus;
import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ClubSessionResponse {
    private String clubTitle;
    private SessionStatus sessionStatus;
    private int sessionCount;
    private double price;
}
