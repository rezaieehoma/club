package com.club.clubservices.club.controller.city.listofallcities;


import com.sportcenter.clubservice.club.service.CityServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ListOfCitiesController {
    private static final Logger logger = LoggerFactory.getLogger( ListOfCitiesController.class );

    private CityServices cityServices;

    @Autowired
    public ListOfCitiesController(CityServices cityServices) {
        this.cityServices = cityServices;
    }

//    @GetMapping(value = "/clubs/cities")

    @GetMapping(value = "/core/users/me/cities")
    public List<ResponseCity> handle() {
        return cityServices.all().stream().map(c -> {
            ResponseCity city = new ResponseCity();
            city.setTitle(c.getTitle());
            return city;
        }).collect(Collectors.toList());
    }
}
