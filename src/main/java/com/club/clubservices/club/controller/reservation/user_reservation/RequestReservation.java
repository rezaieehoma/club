package com.club.clubservices.club.controller.reservation.user_reservation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sportcenter.clubservice.common.converter.TimestampDeserializer;
import lombok.*;

import java.sql.Timestamp;


@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class RequestReservation {
    //private long clubId;

    private long clubTimeHoursId;
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp reservationStartDate;
    private long clubSessionId;
//    @JsonDeserialize(using = TimestampDeserializer.class)
//    private Timestamp reservationEndDate;


}
