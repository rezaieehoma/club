package com.club.clubservices.club.controller.club.clubInfo;

import com.sportcenter.clubservice.club.controller.club.listofsubclubs.ClubTimeResponse;
import com.sportcenter.clubservice.club.controller.club.listofsubclubs.CoachResponse;
import com.sportcenter.clubservice.club.domain.club.Club;
import lombok.*;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ClubResponse {
    private String title;
    private String category;
    private String city;
    private String address;
    private double locationLat;
    private double locationLong;
    private Long memberCount;
    private double monthlyCost;
    private double dailyCost;
    private boolean isComplex;
    private Set<Club> subClubs;
    private List<CoachResponse> coaches;
    private List<String> clubImages;
    private List<ClubTimeResponse> clubTimes;
}
