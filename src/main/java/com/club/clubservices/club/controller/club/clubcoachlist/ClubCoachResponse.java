package com.club.clubservices.club.controller.club.clubcoachlist;

import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ClubCoachResponse {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String title;
    private String description;
    private String category;
    private Integer experienceYear;
    private Integer point;
    private String avatarId;
    private String userStatus;
}
