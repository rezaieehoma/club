package com.club.clubservices.club.controller.club.clubsave;

import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ClubRequest {
    private String title;
    private int categoryId;
    private int cityId;
    private String address;
    private double locationLat;
    private double locationLong;
    private double monthlyCost;
    private double dailyCost;
    //    private boolean isComplex;
    private boolean assortmentSport;
    private long parentClubId;


}
