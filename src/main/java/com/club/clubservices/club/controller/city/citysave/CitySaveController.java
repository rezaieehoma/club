package com.club.clubservices.club.controller.city.citysave;


import com.sportcenter.clubservice.club.controller.ResponseMessage;
import com.sportcenter.clubservice.club.service.CityServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CitySaveController {
    private static final Logger logger = LoggerFactory.getLogger( CitySaveController.class );
    private CityServices cityServices;

    @Autowired
    public CitySaveController(CityServices cityServices) {
        this.cityServices = cityServices;
    }

    @PostMapping(value = "/app_admin/develop/save-city")
    public ResponseMessage handle(@RequestBody CityRequest request) {
        cityServices.save(request.getTitle());

        ResponseMessage message = new ResponseMessage();
        message.setText("A City is added in system");

        return message;
    }
}
