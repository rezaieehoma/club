package com.club.clubservices.club.controller.club.clubcategorysave;


import com.club.clubservices.client.controller.authentication.OnlineUser;
import com.club.clubservices.club.controller.ResponseMessage;
import com.club.clubservices.club.service.ClubCategoryServices;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ClubCategorySaveController {
    private static final Logger logger = LoggerFactory.getLogger(ClubCategorySaveController.class);
    private final ClubCategoryServices clubCategoryServices;
    private final OnlineUser onlineUser;


    @PostMapping("/app_admin/develop/category")
    public ResponseMessage handle(@RequestBody ClubCategoryRequest request) {
        clubCategoryServices.save(request.getTitle());

        ResponseMessage message = new ResponseMessage();
        message.setText("A category is added in system");

        return message;
    }
}
