package com.club.clubservices.club.controller.ClubSession;

import com.sportcenter.clubservice.club.service.ClubSessionServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ClubSessionListController {
    private ClubSessionServices clubSessionServices;

    @Autowired
    public ClubSessionListController(ClubSessionServices clubSessionServices) {
        this.clubSessionServices = clubSessionServices;
    }

    @GetMapping("/users/me/clubsession/list")
    public List<ClubSessionResponse> handle(@RequestParam(value = "clubId") long clubId) {
        return clubSessionServices.finsAllByClubId(clubId).stream().map(clubSession -> {
            return ClubSessionResponse.builder()
                    .clubTitle(clubSession.getClub().getTitle())
                    .sessionStatus(clubSession.getSessionStatus())
                    .sessionCount(clubSession.getSessionCount())
                    .price(clubSession.getPrice())
                    .build();
        }).collect(Collectors.toList());
    }
}
