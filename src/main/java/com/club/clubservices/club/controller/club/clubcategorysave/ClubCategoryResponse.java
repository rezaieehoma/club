package com.club.clubservices.club.controller.club.clubcategorysave;

import lombok.*;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class ClubCategoryResponse {
    private String title;
}
