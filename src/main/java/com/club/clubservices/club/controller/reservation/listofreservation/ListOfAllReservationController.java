package com.club.clubservices.club.controller.reservation.listofreservation;

import com.sportcenter.clubservice.club.service.ClubSessionServices;
import com.sportcenter.clubservice.club.service.ReservationServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ListOfAllReservationController {
    private ReservationServices reservationServices;
    private ClubSessionServices clubSessionServices;
    private OnlineUser onlineUser;


    @Autowired
    public ListOfAllReservationController(ReservationServices reservationServices, ClubSessionServices clubSessionServices, OnlineUser onlineUser) {
        this.reservationServices = reservationServices;
        this.clubSessionServices = clubSessionServices;
        this.onlineUser = onlineUser;
    }

    @GetMapping("/clubs/reservation/list")
    public List<ReservationResponse> handle() {

        List<ReservationResponse> collect = reservationServices.listOfAllReservationClub(
                onlineUser.getUserId()).stream().map(reserve -> {
            ReservationResponse response = ReservationResponse.builder()
                    .firstName(reserve.getUserId().getFirstName())
                    .lastName(reserve.getUserId().getLastName())
                    .reservationStartDate(reserve.getReservationStartDate())
                    .reservationEndDate(reserve.getReservationEndDate())
                    .sessionCount(reserve.getClubSessionId() != null ? reserve.getClubSessionId().getSessionCount() : 0)


                    //.sessionCount( reserve.getClubSessionId().getSessionCount() )

                    .status( reserve.getStatus() )
                    .build();

            return response;
        } ).collect( Collectors.toList() );
        return collect;
    }
}
