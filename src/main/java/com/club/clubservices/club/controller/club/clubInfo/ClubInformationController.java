package com.club.clubservices.club.controller.club.clubInfo;

import com.sportcenter.clubservice.club.controller.club.listofsubclubs.ClubTimeResponse;
import com.sportcenter.clubservice.club.controller.club.listofsubclubs.CoachResponse;
import com.sportcenter.clubservice.club.domain.club.Club;
import com.sportcenter.clubservice.club.service.ClubImageServices;
import com.sportcenter.clubservice.club.service.ClubServices;
import com.sportcenter.clubservice.club.service.ClubTimeHoursServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class ClubInformationController {
    private final ClubServices clubServices;
    private final ClubImageServices clubImageServices;
    private final OnlineUser onlineUser;
    private final ClubTimeHoursServices clubTimeHoursServices;


    @GetMapping(value = "/clubs/clubinfo")
    public ClubResponse handle() {
        Club club = clubServices.getClubInformation(onlineUser.getUserId());
        List<String> collect = clubImageServices.findImageByClubId(club.getId())
                .stream().map(image -> image.getAvatarId()).collect(Collectors.toList());

        return ClubResponse.builder()
                .title(club.getTitle())
                .category(club.getCategory().getTitle())
                .city(club.getCity().getTitle())
                .address(club.getAddress())
                .locationLat(club.getLocationLat())
                .locationLong(club.getLocationLong())
                .memberCount(club.getMemberCount())
                .monthlyCost(club.getMonthlyCost())
                .dailyCost(club.getDailyCost())
                .isComplex(club.isComplex())
                .subClubs(null)
                .clubImages(collect == null ? null : collect)
                .coaches(club.getClubCoaches().stream().map(c -> {
                    CoachResponse couch = CoachResponse.builder()
                            .firstName(c.getCoach().getUserId().getFirstName())
                            .lastName(c.getCoach().getUserId().getLastName())
                            .avatarId(c.getCoach().getAvatarId())
                            .title(c.getCoach().getTitle())
                            .description(c.getCoach().getDescription())
                            .category(c.getCoach().getCategory())
                            .experienceYear(c.getCoach().getExperienceYear())
                            .point(c.getCoach().getPoint())
                            .avatarId(c.getCoach().getUserId().getAvatarId())
                            .build();
                    return couch;
                }).collect(Collectors.toList()))
                .clubTimes(clubTimeHoursServices.findAllClubTime(club.getId()).stream().map(time -> {
                    ClubTimeResponse build = ClubTimeResponse.builder()
                            .startTime(time.getStartTime())
                            .endTime(time.getEndTime())
                            .title(time.getTitle())
                            .timeStatus(time.getTimeStatus().name())
                            .price(time.getPrice())
                            .build();
                    return build;
                }).collect(Collectors.toList()))
                .build();

    }
}
