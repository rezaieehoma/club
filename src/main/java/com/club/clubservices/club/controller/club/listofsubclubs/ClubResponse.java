package com.club.clubservices.club.controller.club.listofsubclubs;


import lombok.*;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ClubResponse {
    private String title;
    private int categoryId;
    private int cityId;
    private String address;
    private Double locationLat;
    private Double locationLong;
    private Double monthlyCost;
    private Double dailyCost;
    private List<String> avatarId;
    private List<CoachResponse> coaches;
    private Set<ClubResponse> subClubs;
    private List<ClubTimeResponse> clubTimes;


}
