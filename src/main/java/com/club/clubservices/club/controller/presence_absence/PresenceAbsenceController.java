package com.club.clubservices.club.controller.presence_absence;

import com.sportcenter.clubservice.club.controller.ResponseMessage;
import com.sportcenter.clubservice.club.domain.presence_absence.PresenceAbsence;
import com.sportcenter.clubservice.club.service.PresenceAbsenceServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PresenceAbsenceController {
    private PresenceAbsenceServices presenceAbsenceServices;
    private OnlineUser onlineUser;

    @Autowired
    public PresenceAbsenceController(PresenceAbsenceServices presenceAbsenceServices, OnlineUser onlineUser) {
        this.presenceAbsenceServices = presenceAbsenceServices;
        this.onlineUser = onlineUser;
    }

    @PostMapping("/clubs/presence/absence")
    public ResponseMessage handle(@RequestBody PresenceAbsenceRequest request) {
//        PresenceAbsence presenceAbsence = presenceAbsenceServices.save( onlineUser.getUserId(),
//                request.getReservationDate(), request.getReservationStartTime() );
        ResponseMessage message = new ResponseMessage();

        PresenceAbsence presenceAbsence = presenceAbsenceServices.save(request.getReservationId());
        message.setText(presenceAbsence.getUserId().getFirstName() + " " +
                presenceAbsence.getUserId().getLastName() + " is presence");
        return message;
    }


}
