package com.club.clubservices.club.controller.club.clubcoachlist;

import com.club.clubservices.client.controller.authentication.OnlineUser;
import com.club.clubservices.coach.service.ClubCoachService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class ClubCoachListController {
    private final OnlineUser onlineUser;
    private final ClubCoachService clubCoachService;
   // private final UserServices userServices;
//user.getPhoneNumber()
    @GetMapping(value = "/clubs/coaches")
    public List<ClubCoachResponse> handle() {
        //User user = userServices.findUserById(onlineUser.getUserId());
        return clubCoachService.findCoachByClubId(onlineUser.getUserId());

    }
}
