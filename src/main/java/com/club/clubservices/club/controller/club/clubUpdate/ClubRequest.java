package com.club.clubservices.club.controller.club.clubUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClubRequest {
    private String title;
    private double monthlyCost;
    private double dailyCost;
    private boolean assortmentSport;
}
