package com.club.clubservices.club.controller.time;

import com.sportcenter.clubservice.club.controller.ResponseMessage;
import com.sportcenter.clubservice.club.exception.time.ClubTimeRequestIsEmptyException;
import com.sportcenter.clubservice.club.service.ClubTimeHoursServices;
import com.sportcenter.clubservice.config.security.OnlineUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClubTimeHoursController {
    private static final Logger logger = LoggerFactory.getLogger( ClubTimeHoursController.class );

    private ClubTimeHoursServices clubTimeHoursServices;
    private OnlineUser onlineUser;

    @Autowired
    public ClubTimeHoursController(ClubTimeHoursServices clubTimeHoursServices, OnlineUser onlineUser) {
        this.clubTimeHoursServices = clubTimeHoursServices;
        this.onlineUser = onlineUser;
    }

    @PostMapping("/clubs/clubtimehours")
    public ResponseMessage handle(@RequestBody ClubTimeHoursRequest request) {
        if (request == null) {
            logger.info("club time is empty");
            throw new ClubTimeRequestIsEmptyException();
        }
        clubTimeHoursServices.save(onlineUser.getUserId(), request.getStartTime()
                , request.getEndTime(), request.getTitle(), request.getTimeStatus(), request.getPrice());

        ResponseMessage message = new ResponseMessage();
        message.setText("set club time ");
        return message;
    }
}
