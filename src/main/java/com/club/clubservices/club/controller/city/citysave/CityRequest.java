package com.club.clubservices.club.controller.city.citysave;

public class CityRequest {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
