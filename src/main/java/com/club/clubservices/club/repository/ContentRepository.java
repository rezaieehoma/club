package com.club.clubservices.club.repository;


import com.club.clubservices.club.domain.image.Content;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContentRepository extends JpaRepository<Content, Long> {
    Optional<Content> findById(long id);

//    @Query("FROM Content where user=?1 and club=null")
//    Content findByUser(long user);
}
