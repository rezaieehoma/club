package com.club.clubservices.club.repository;


import com.club.clubservices.club.domain.ClubCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClubCategoryRepository extends JpaRepository<ClubCategory, Integer> {
    Optional<ClubCategory> findByTitle(String title);

}
