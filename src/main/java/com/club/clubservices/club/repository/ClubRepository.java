package com.club.clubservices.club.repository;


import com.club.clubservices.club.domain.City;
import com.club.clubservices.club.domain.Club;
import com.club.clubservices.club.domain.ClubCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClubRepository extends JpaRepository<Club, Long> {
    Optional<Club> findByTitle(String title);

    Club getByTitle(String title);

    @Query("SELECT c FROM Club  c WHERE (:category is null or c.category = :category) and (:city is null or c.city = :city )")
    List<Club> findAllByCategoryAndCity(@Param("category") ClubCategory category, @Param("city") City city);
}
