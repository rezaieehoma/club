package com.club.clubservices.club.repository;



import com.club.clubservices.club.domain.Club;
import com.club.clubservices.club.domain.session.ClubSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClubSessionRepository extends JpaRepository<ClubSession, Long> {
    ClubSession findById(long clubSessionId);

    List<ClubSession> findAllByClub(Club club);
}
