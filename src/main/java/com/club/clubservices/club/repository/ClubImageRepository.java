package com.club.clubservices.club.repository;


import com.club.clubservices.club.domain.Club;
import com.club.clubservices.club.domain.image.ClubImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClubImageRepository extends JpaRepository<ClubImage, Long> {
    List<ClubImage> findAllByClubId(long clubId);

    List<ClubImage> findAllByClub(Club club);


}
