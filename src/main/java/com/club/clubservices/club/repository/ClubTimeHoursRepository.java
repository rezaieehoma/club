package com.club.clubservices.club.repository;

import com.club.clubservices.club.domain.Club;
import com.club.clubservices.club.domain.time.ClubTimeHours;
import com.club.clubservices.common.type.TimeStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Time;
import java.util.List;

@Repository
public interface ClubTimeHoursRepository extends JpaRepository<ClubTimeHours, Long> {
    ClubTimeHours findByStartTime(Time startTime);

    ClubTimeHours findById(long id);

    List<ClubTimeHours> findAllByClubAndTimeStatus(Club club, TimeStatus status);

    ClubTimeHours findByClubAndStartTime(Club club, Time startTime);

    List<ClubTimeHours> findAllById(long id);



}
