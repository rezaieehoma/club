package com.club.clubservices.club.domain.image;


import com.club.clubservices.club.domain.Club;
import com.club.clubservices.common.type.ImageCategory;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClubImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "club_id")
    private Club club;

    private String avatarId;
    @Enumerated(EnumType.STRING)
    private ImageCategory imageCategory;
    private LocalDateTime createDate;

}
