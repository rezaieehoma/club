package com.club.clubservices.club.domain.image;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UploadFileInfo {
    private String id;
    private String url;
}
