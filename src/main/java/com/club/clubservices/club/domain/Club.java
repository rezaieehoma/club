package com.club.clubservices.club.domain;


import com.club.clubservices.club.domain.image.ClubImage;
import com.club.clubservices.club.domain.time.ClubTimeHours;
import com.club.clubservices.coach.domain.coach.ClubCoach;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Setter
@Getter
//@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class Club {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    private String title;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private ClubCategory category;
    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;
    private String address;
    private double locationLat;
    private double locationLong;
    private Long memberCount;
    private double monthlyCost;
    private double dailyCost;
    private boolean isComplex;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_category_id")
    private Club parentClub;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentClub")
    private Set<Club> subClubs;

    @OneToMany(mappedBy = "club")
    private Set<ClubTimeHours> clubTimes;

//    @ManyToMany(cascade = {CascadeType.ALL})
//    @JoinTable(name = "club_couch", joinColumns = {@JoinColumn(name = "club_id")},
//            inverseJoinColumns = {@JoinColumn(name = "couch_id")})
//    private Set<Couch> couches;

    @OneToMany(mappedBy = "club")
    Set<ClubCoach> clubCoaches;
    @OneToMany(mappedBy = "club")
    Set<ClubImage> clubImages;


    private LocalDateTime createDate;


}
