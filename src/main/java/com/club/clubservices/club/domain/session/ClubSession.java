package com.club.clubservices.club.domain.session;


;
import com.club.clubservices.club.domain.Club;
import com.club.clubservices.common.type.SessionStatus;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ClubSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "club_id")
    private Club club;
    @Enumerated(EnumType.STRING)
    private SessionStatus sessionStatus;
    private int sessionCount;
    private double price;
    private Timestamp createDate;
}
