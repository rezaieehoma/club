package com.club.clubservices.club.domain.image;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Content {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //    @ManyToOne
//    @JoinColumn(name = "club_id")
//    private Club club;
//    @ManyToOne
//    @JoinColumn(name = "user_id")
//    private User user;
    private String storagePath;
    private String fileType;
    private int fileSize;
    private LocalDateTime createDate;
}
