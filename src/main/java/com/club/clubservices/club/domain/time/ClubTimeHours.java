package com.club.clubservices.club.domain.time;

import com.club.clubservices.club.domain.Club;
import com.club.clubservices.common.type.TimeStatus;
import lombok.*;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDateTime;


@Entity
@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class ClubTimeHours {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "club_id")
    private Club club;
    private Time startTime;
    private Time endTime;
    private String title;
    @Enumerated(EnumType.STRING)
    private TimeStatus timeStatus;
    private double price;
    private LocalDateTime createDate;


}
