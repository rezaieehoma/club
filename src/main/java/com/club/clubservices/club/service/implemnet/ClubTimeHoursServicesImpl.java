package com.club.clubservices.club.service.implemnet;


import com.club.clubservices.club.domain.Club;
import com.club.clubservices.club.domain.time.ClubTimeHours;
import com.club.clubservices.club.repository.ClubRepository;
import com.club.clubservices.club.repository.ClubTimeHoursRepository;
import com.club.clubservices.club.service.ClubTimeHoursServices;
import com.club.clubservices.clubadmin.domain.ClubAdmin;
import com.club.clubservices.clubadmin.service.ClubAdminService;
import com.club.clubservices.common.type.TimeStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class ClubTimeHoursServicesImpl implements ClubTimeHoursServices {
    private static final Logger logger = LoggerFactory.getLogger( ClubTimeHoursServicesImpl.class );

    private ClubTimeHoursRepository clubTimeHoursRepository;
    private ClubRepository clubRepository;
    private ClubAdminService clubAdminService;
    private UserServices userServices;

    @Autowired
    public ClubTimeHoursServicesImpl(ClubTimeHoursRepository clubTimeHoursRepository,
                                     ClubRepository clubRepository, ClubAdminService clubAdminService, UserServices userServices) {
        this.clubTimeHoursRepository = clubTimeHoursRepository;
        this.clubRepository = clubRepository;
        this.clubAdminService = clubAdminService;
        this.userServices = userServices;
    }

    @Override
    public void save(long userId, Time startDate, Time endDate,
                     String title, TimeStatus timeStatus, double price) {
        ClubTimeHours builder = ClubTimeHours.builder()
                .club( findClubByUserId( userId ) )
                .startTime( startDate )
                .endTime( endDate )
                .title( title )
                .timeStatus( timeStatus )
                .price( price )
                .createDate( LocalDateTime.now() )
                .build();
        clubTimeHoursRepository.save( builder );

    }

    @Override
    public ClubTimeHours findByStartTimeAndDateTime(Time time, Time dateTime) {
        return clubTimeHoursRepository.findByStartTime( time );

    }

    @Override
    public List<ClubTimeHours> findAllClubTime(long clubId) {
        Club club = findClubById( clubId );
        return clubTimeHoursRepository.findAllByClubAndTimeStatus( club, TimeStatus.ON );
    }

    @Override
    public ClubTimeHours findByCLubAndStartTimeAndDateTime(long clubId, Time time, Time dateTime) {
        Club club = findClubById( clubId );
        return clubTimeHoursRepository.findByClubAndStartTime( club, time );
    }

    private Club findClubById(long id) {
        return clubRepository.findById(id).get();
    }

    private Club findClubByUserId(long userId) {
        User user = userServices.getProfile( userId );
        ClubAdmin clubAdmin = clubAdminService.findByPhoneNumber( user.getPhoneNumber() );
        return clubAdmin.getClub();
    }

}
