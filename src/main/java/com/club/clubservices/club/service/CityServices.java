package com.club.clubservices.club.service;


import com.club.clubservices.club.domain.City;

import java.util.List;

public interface CityServices {
    void save(String title);

    City findCityById(int id);

    List<City> all();

}
