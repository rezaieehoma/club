package com.club.clubservices.club.service.implemnet;


import com.club.clubservices.club.domain.Club;
import com.club.clubservices.club.domain.image.ClubImage;
import com.club.clubservices.club.repository.ClubRepository;
import com.club.clubservices.club.service.CityServices;
import com.club.clubservices.club.service.ClubCategoryServices;
import com.club.clubservices.club.service.ClubImageServices;
import com.club.clubservices.club.service.ClubServices;
import com.club.clubservices.clubadmin.domain.ClubAdmin;
import com.club.clubservices.clubadmin.service.ClubAdminService;
import com.club.clubservices.coach.service.ClubCoachService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ClubServicesImpl implements ClubServices {
    private static final Logger logger = LoggerFactory.getLogger(ClubServicesImpl.class);
    private final ClubRepository clubRepository;
    private final ClubCategoryServices clubCategoryServices;
    private final CityServices cityServices;
    private final UserServices userServices;
    private final ClubAdminService clubAdminService;
    private final ClubImageServices clubImageServices;
    private final ClubCoachService clubCoachService;
    //private final UploadFileClient uploadFileClient;


    @Override
    public void save(String title, int categoryId, String address,
                     int cityId, double locationLat, double locationLong,
                     double monthlyCost, double daily_cost, boolean isComplex, long parentCategoryId) {

        if (clubRepository.findByTitle(title).orElse(null) != null) {
            logger.info("This club already exist!!!");
            throw new ClubAlreadyExistException();
        }
        Club clubParent = clubRepository.findById(parentCategoryId).orElse(null);
        //repository.findById(parentCategoryId).get();
        Club club = Club.builder().title(title)
                .category(clubCategoryServices.findClubCategoryById(categoryId))
                .city(cityServices.findCityById(cityId))
                .address(address)
                .locationLat(locationLat)
                .locationLong(locationLong)
                .monthlyCost(monthlyCost)
                .dailyCost(daily_cost)
                .isComplex(isComplex)
                .parentClub(clubParent == null ? null : clubParent)
                .memberCount( (long) 0 )
                .createDate( LocalDateTime.now() )
                .build();

        clubRepository.save(club);

    }


    @Override
    public Club findClubById(long clubId) {
        //return repository.findById(clubId).get();
        return clubRepository.findById(clubId).get();

    }

    @Override
    public Set<Club> getClub(long userId) {
        User user = userServices.getProfile(userId);
        ClubAdmin clubAdmin = clubAdminService.findByPhoneNumber(user.getPhoneNumber());
        Set<Club> subClubs = clubAdmin.getClub().getSubClubs();
        List<ClubCoach> collect = subClubs.stream()
                .map(subClub -> clubCoachService.findCouchByClub(subClub))
                .collect(Collectors.toList());
        return subClubs;
    }

    @Override
    public List<Club> findAll() {
        return clubRepository.findAll();
    }


    @Override
    public List<Club> listOfClubCategory(Integer categoryId, Integer cityId) {
        //throw new RuntimeException( "Custom error" );

        List<Club> allByCategoryAndCity
                = clubRepository.findAllByCategoryAndCity(categoryId != null ?
                        clubCategoryServices.findClubCategoryById(categoryId) : null,
                cityId != null ? cityServices.findCityById(cityId) : null);

//        allByCategoryAndCity.forEach(
//                club -> {
//                    club.getClubCouches().forEach(
//                            couch -> {
//                                couch.getCouch().setImageUrl( uploadFileService.getImageUrl( couch.getCouch().getAvatars() ) );
//                            }
//                    );
//                }
//        );
        return allByCategoryAndCity;
    }

    @Override
    public Club getClubInformation(long adminId) {
        User user = userServices.getProfile(adminId);
        ClubAdmin clubAdmin = clubAdminService.findByPhoneNumber(user.getPhoneNumber());
        return clubAdmin.getClub();
    }

    @Override
    public Club updateClub(String title, double monthlyCost, double dailyCost, long clubId) {
        Club club = clubRepository.findById(clubId).orElseThrow(ClubCanNotBeFoundException::new);

        club.setTitle(title);
        club.setDailyCost(dailyCost);
        club.setMonthlyCost(monthlyCost);

        return club;
    }

    @Override
    public void updateAvatar(long userId, String avatarId) {
        Club club = getClubInformation(userId);
        List<ClubImage> imageByClubId = clubImageServices.findImageByClubId(club.getId());
        ClubImage clubImage = new ClubImage();
        clubImage.setAvatarId(avatarId);

    }

//    @Override
//    public UploadFileInfo uploadFileClub(long id, MultipartFile file, String imageCategory) {
//        User user = userServices.findUserById(id);
//        ClubAdmin clubAdmin = clubAdminService.findByPhoneNumber(user.getPhoneNumber());
//        UploadFileClientResponse info = uploadFileClient.sendUploadFileInfo(file, imageCategory);
//
//        clubImageServices.updateAvatar(clubAdmin.getClub().getId(), info.getId(), imageCategory);
//        return UploadFileInfo.builder().url(info.getUrl()).id(info.getId()).build();
//    }


}
