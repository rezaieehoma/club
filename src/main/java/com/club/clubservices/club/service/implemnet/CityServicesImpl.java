package com.club.clubservices.club.service.implemnet;

import com.sportcenter.clubservice.club.domain.City;
import com.sportcenter.clubservice.club.exception.city.CityAlreadyExistException;
import com.sportcenter.clubservice.club.repository.CityRepository;
import com.sportcenter.clubservice.club.service.CityServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;


@Service
@Transactional
public class CityServicesImpl implements CityServices {
    private static final Logger logger = LoggerFactory.getLogger( CityServicesImpl.class );
    private CityRepository cityRepository;

    @Autowired
    public CityServicesImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public void save(String title) {

        if (cityRepository.findByTitle(title).orElse(null) == null) {
            City city = City.builder().title(title).createDate(LocalDateTime.now()).build();
            cityRepository.save(city);
        } else {
            logger.info("This city alreday exist");
            throw new CityAlreadyExistException();
        }

    }

    @Override
    public City findCityById(int id) {
        return cityRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public List<City> all() {
        return cityRepository.findAll();
    }

}
