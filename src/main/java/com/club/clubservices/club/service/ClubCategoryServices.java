package com.club.clubservices.club.service;



import com.club.clubservices.club.domain.ClubCategory;

import java.util.List;
import java.util.Optional;

public interface ClubCategoryServices {
    void save(String title);

    List<ClubCategory> all();

    Optional<ClubCategory> findById(int id);

    ClubCategory findClubCategoryById(int id);

}
