package com.club.clubservices.club.service;



import com.club.clubservices.club.domain.image.ClubImage;

import java.util.List;

public interface ClubImageServices {
    List<ClubImage> findImageByClubId(long clubId);

    void updateAvatar(long clubId, String avatarId, String imageCategory);

}
