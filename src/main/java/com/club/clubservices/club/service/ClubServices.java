package com.club.clubservices.club.service;

import com.club.clubservices.club.domain.Club;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

public interface ClubServices {
    void save(String title, int categoryId, String address,
              int cityId,
              double locationLat, double locationLong,
              double monthlyCost, double daily_cost, boolean isComplex, long parentCategoryId);

    Club findClubById(long clubId);

    Set<Club> getClub(long userId);

    List<Club> findAll();


    List<Club> listOfClubCategory(Integer categoryId, Integer cityId);

    Club getClubInformation(long adminId);

    Club updateClub(String title, double monthlyCost, double dailyCost, long clubId);

    void updateAvatar(long userId, String avatarId);

  //  UploadFileInfo uploadFileClub(long id, MultipartFile file, String imageCategory);
}
