package com.club.clubservices.club.service;



import com.club.clubservices.club.domain.time.ClubTimeHours;
import com.club.clubservices.common.type.TimeStatus;

import java.sql.Time;
import java.util.List;

public interface ClubTimeHoursServices {
    void save(long userId, Time startDate, Time endDate,
              String title, TimeStatus timestatus, double price);

    ClubTimeHours findByStartTimeAndDateTime(Time time, Time dateTime);

    List<ClubTimeHours> findAllClubTime(long clubId);


    ClubTimeHours findByCLubAndStartTimeAndDateTime(long clubId, Time time, Time dateTime);
}
