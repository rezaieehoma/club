package com.club.clubservices.club.service.implemnet;


import com.club.clubservices.club.domain.Club;
import com.club.clubservices.club.domain.image.ClubImage;
import com.club.clubservices.club.repository.ClubImageRepository;
import com.club.clubservices.club.repository.ClubRepository;
import com.club.clubservices.club.service.ClubImageServices;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;


@Service
@Transactional
@RequiredArgsConstructor
public class ClubImageServicesImpl implements ClubImageServices {
    private final ClubImageRepository clubImageRepository;
    private final ClubRepository clubRepository;


    @Override
    public List<ClubImage> findImageByClubId(long clubId) {
        Club club = clubRepository.findById(clubId).orElse(null);
        List<ClubImage> list = clubImageRepository.findAllByClubId(club.getId());
        return list;
    }

    @Override
    public void updateAvatar(long clubId, String avatarId, String imageCategory) {
        Club club = clubRepository.findById(clubId).orElseThrow(ClubCanNotBeFoundException::new);
        clubImageRepository.save(ClubImage.builder()
                .club(club)
                .avatarId(avatarId)
                .imageCategory(ImageCategory.valueOf(imageCategory))
                .createDate(LocalDateTime.now())
                .build());
    }

}
