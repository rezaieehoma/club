package com.club.clubservices.club.service.implemnet;


import com.club.clubservices.club.domain.ClubCategory;
import com.club.clubservices.club.repository.ClubCategoryRepository;
import com.club.clubservices.club.service.ClubCategoryServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ClubCategoryServicesImpl implements ClubCategoryServices {
    private static final Logger logger = LoggerFactory.getLogger( ClubCategoryServicesImpl.class );
    private ClubCategoryRepository clubCategoryRepository;

    @Autowired
    public ClubCategoryServicesImpl(ClubCategoryRepository clubCategoryRepository) {
        this.clubCategoryRepository = clubCategoryRepository;
    }

    @Override
    public void save(String title) {
        if (clubCategoryRepository.findByTitle( title ).orElse( null ) == null) {
            ClubCategory clubCategory = new ClubCategory();

            clubCategory.setTitle( title );
            clubCategoryRepository.save( clubCategory );

        } else {
            logger.info( "This club category already exist" );
            throw new ClubCategoryAlreadyExistException();
        }

    }

    @Override
    public List<ClubCategory> all() {
        List<ClubCategory> all = clubCategoryRepository.findAll();
        return all;
    }

    @Override
    public Optional<ClubCategory> findById(int id) {

        return clubCategoryRepository.findById(id);
    }

    @Override
    public ClubCategory findClubCategoryById(int id) {
        return clubCategoryRepository.findById(id).orElseThrow(RuntimeException::new);
    }
}
